// -----------Get the popup
var popup = document.getElementById("imgPopup");
var popupImg = document.getElementById("imgSelect");
var prevBtn = document.getElementById("prevBtn");
var nextBtn = document.getElementById("nextBtn");
var currentImageIndex = 0;

popup.style.display = "none";
// Get the images and assign an onclick events
var imgs = document.getElementsByClassName("gallery")[0].getElementsByTagName("img");

for (var i = 0; i < imgs.length; i++) {
    imgs[i].onclick = function () {
        currentImageIndex = Array.from(imgs).indexOf(this);
        displayImage(currentImageIndex);
    }
}


// Function to display an image by index
function displayImage(index) {
    popup.style.display = "block";
    popupImg.src = imgs[index].src;
    currentImageIndex = index;
}

// Close the modal when the close button is clicked
var closeBtn = document.getElementsByClassName("close")[0];
closeBtn.onclick = function () {
    popup.style.display = "none";
}

window.addEventListener("click", function (event) {
    if (event.target == popup) {
        closePopUp();
    }
});
function closePopUp() {
    popup.style.display = "none";
}

// Go to the previous image
prevBtn.onclick = function () {
    currentImageIndex = (currentImageIndex - 1 + imgs.length) % imgs.length;
    displayImage(currentImageIndex);
}

// Go to the next image
nextBtn.onclick = function () {
    currentImageIndex = (currentImageIndex + 1) % imgs.length;
    displayImage(currentImageIndex);
}

//-----------------------Music Control
//-----------------------Music Control
const musicIcon = document.getElementById('music-icon');
const audio = document.getElementById('audio');
const statusText = document.getElementById('status-text');

let isPlaying = false;

// Ẩn status-text sau 5 giây
setTimeout(() => {
    statusText.style.display = 'none';
  }, 8000);

musicIcon.addEventListener('click', function() {
  if (isPlaying) {
    audio.pause();
    musicIcon.classList.remove('fa-volume-up'); 
    musicIcon.classList.add('fa-volume-mute'); 
    // statusText.style.display = 'block';
    
  } else {
    audio.play();
    musicIcon.classList.remove('fa-volume-mute'); 
    musicIcon.classList.add('fa-volume-up');
    // statusText.style.display = 'none';
  }
  isPlaying = !isPlaying;
  console.log(isPlaying);
});

// -------------Canvas heart falling
setInterval(createHeart, 500);

function createHeart() {
    const heart = document.createElement('i');
    heart.classList.add('fas');
    heart.classList.add('fa-heart');
    heart.classList.add('heart-fall');
    heart.classList.add('heart-fall-no-interaction');
    heart.style.left = Math.random() * window.innerWidth + 'px';
    heart.style.animationDuration = Math.random() * 3 + 2 + 's'; //seconds falling
    heart.style.opacity = Math.random();
    heart.style.fontSize = Math.random() * 10 + 10 + 'px';

    document.body.appendChild(heart);

    setTimeout(() => {
        heart.remove();
    }, 5000);
}
